import "../styles/globals.scss";
import "../styles/learn.scss";

function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />;
}

export default MyApp;
