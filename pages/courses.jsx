import Header from "../Components/Header/Header";
import Head from "next/head";
import AllCourses from "../Components/OnlineBlock/CouresBlock";
import styles from "../styles/Home.module.scss";

export default function Courses() {
  return (
    <div>
      <Head>
        <meta name="keywords" content="next javascript react" />
        <title>Courses</title>
      </Head>
      <Header />
      <div className={styles.padding_curs}>
        <AllCourses />
      </div>
    </div>
  );
}
