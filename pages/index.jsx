import Header from "../Components/Header/Header";

import Main from "../Components/TranformBlock/Main";
import Head from "next/head";
import ItWork from "../Components/HowIt/Learn";
import AllCourses from "../Components/OnlineBlock/CouresBlock";
import BlackB from "../Components/BlackBlock/BlockB";
import BlockInFo from "../Components/BlockStart/BlockIn";

export default function Home() {
  return (
    <div className="screen-wrapper-home">
      <Head>
        <meta name="keywords" content="next javascript react" />
        <title>Home</title>
      </Head>
      <Header />
      <Main />
      <ItWork />
      <AllCourses />
      <BlackB />
      <BlockInFo />
    </div>
  );
}
