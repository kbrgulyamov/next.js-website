import { useRouter } from "next/router";
import Header from "../../Components/Header/Header";

export default function () {
  const { query } = useRouter();
  console.log(query);
  return (
    <>
      <Header />
      <h1 style={{ padding: "90px" }}>Courses id: {query.id}</h1>;
    </>
  );
}
