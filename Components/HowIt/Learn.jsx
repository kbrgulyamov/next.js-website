import React from "react";
import Head from "next/head";
import Image from "next/image";
import styles from "../../styles/Home.module.scss";
import one from "../HowIt/img/1.svg";
import two from "../HowIt/img/2.svg";
import three from "../HowIt/img/3.svg";
import four from "../HowIt/img/4.svg";

const ItWork = () => {
  return (
    <div className={styles.container_it}>
      <div className={styles.title}>
        <h1>How It Work</h1>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit.
          <br /> Aliquam et hendrerit euismod fusce sit.
        </p>
      </div>
      <div className={styles.img_flex}>
        <div className={styles.img_wrapper}>
          <Image src={one} />
          <p>
            Lorem ipsum dolor sit amet consectetur
            <br /> adipisicing. Perspiciatis, magni?
          </p>
        </div>
        <div className={styles.img_wrapper}>
          <Image src={two} />
          <p>
            Lorem ipsum dolor sit amet consectetur
            <br /> adipisicing. Perspiciatis, magni?
          </p>
        </div>
        <div className={styles.img_wrapper}>
          <Image src={three} />
          <p>
            Lorem ipsum dolor sit amet consectetur
            <br /> adipisicing. Perspiciatis, magni?
          </p>
        </div>
        <div className={styles.img_wrapper}>
          <Image src={four} />
          <p>
            Lorem ipsum dolor sit amet consectetur
            <br /> adipisicing. Perspiciatis, magni?
          </p>
        </div>
      </div>
    </div>
  );
};

export default ItWork;
