import React from "react";
import styles from "../../styles/Home.module.scss";
import Link from "next/link";

const BlockInFo = () => {
  return (
    <div className={styles.container_down}>
      <div className={styles.flex_contaent}>
        <div className={styles.contaent_left}>
          <div className={styles.img_left}>
            <img
              src="https://wepro.academy/_nuxt/img/dots.819d6e9.svg"
              alt=""
              className={styles.insaet_img}
            />
            <div className={styles.box_cards}>
              <div className={styles.block_card}>
                {/* Cards Learn */}
                <Link href={"learn/ComputerS"}>
                  <a>
                    <div className={styles.cards_product}>
                      <div className={styles.head_card}>
                        <div className={styles.pro_bg}>
                          <p>PRO</p>
                        </div>
                        <p className={styles.p}>Career Path</p>
                      </div>
                      <div className={styles.name_learn}>
                        <p>Computer Scines</p>
                        <div className={styles.p_info}>
                          <div className={styles.cericle}></div>
                          <p>Beginner friendly, 82 Lessons</p>
                        </div>
                      </div>
                      <div className={styles.bottom_box_wrap}>
                        <div className={styles.card_box}>
                          <p>Foundations</p>
                        </div>
                      </div>
                    </div>
                  </a>
                </Link>
                <Link href={"/learn/DataScines"}>
                  <a>
                    <div className={styles.cards_product}>
                      <div className={styles.head_card}>
                        <div className={styles.pro_bg}>
                          <p>PRO</p>
                        </div>
                        <p className={styles.p}>Career Path</p>
                      </div>
                      <div className={styles.name_learn}>
                        <p>Data Scines</p>
                        <div className={styles.p_info}>
                          <div className={styles.cericle}></div>
                          <p>Beginner friendly, 78 Lessons</p>
                        </div>
                      </div>
                      <div className={styles.bottom_box_wrap}>
                        <div className={styles.card_box}>
                          <p>Job Found</p>
                        </div>
                      </div>
                    </div>
                  </a>
                </Link>
              </div>
              <div className={styles.column_two}>
                {/* Cards Learn */}
                <Link href={"learn/FullStack"}>
                  <a>
                    <div className={styles.cards_product_2}>
                      <div className={styles.head_card}>
                        <div className={styles.pro_bg}>
                          <p>PRO</p>
                        </div>
                        <p className={styles.p}>Career Path</p>
                      </div>
                      <div className={styles.name_learn}>
                        <p>Full-Stack Enginner</p>
                        <div className={styles.p_info}>
                          <div className={styles.cericle}></div>
                          <p>Beginner friendly, 82 Lessons</p>
                        </div>
                      </div>
                      <div className={styles.bottom_box_wrap}>
                        <div className={styles.card_box}>
                          <p>Foundations</p>
                        </div>
                      </div>
                    </div>
                  </a>
                </Link>
                <Link href={"/learn/Cybersecurity"}>
                  <a>
                    <div className={styles.cards_product_2}>
                      <div className={styles.head_card}>
                        <div className={styles.pro_bg}>
                          <p>PRO</p>
                        </div>
                        <p className={styles.p}>Career Path</p>
                      </div>
                      <div className={styles.name_learn}>
                        <p>Cybersecurity</p>
                        <div className={styles.p_info}>
                          <div className={styles.cericle}></div>
                          <p>Beginner friendly, 82 Lessons</p>
                        </div>
                      </div>
                      <div className={styles.bottom_box_wrap}>
                        <div className={styles.card_box}>
                          <p>Foundations</p>
                        </div>
                      </div>
                    </div>
                  </a>
                </Link>
              </div>
            </div>
          </div>
        </div>
        <div className={styles.right_container}>
          <h1>What’s your goal?</h1>
          <img src="https://careertail.io/assets/possible-sect-image1.png" alt="" />
        </div>
      </div>
    </div>
  );
};

export default BlockInFo;

{
  /* */
}
