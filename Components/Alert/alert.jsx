import React from "react";
import styles from "../../styles/Home.module.scss";

const Alert = () => {
  return (
    <div className={styles.container_alert}>
      <div className={styles.alert_wrapper}>
        <div className={styles.alert_danger}></div>
      </div>
    </div>
  );
};

export default Alert;
