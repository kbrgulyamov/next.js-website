import React from "react";
import styles from "../../styles/Home.module.scss";
import Logo from "../Header/log/logo.svg";
import Image from "next/image";
import Link from "next/link";
import Head from "next/head";
import shop from "../Header/log/Frame (6).svg";

const Header = () => {
  return (
    <>
      <header className={styles.header}>
        <div className={styles.container}>
          <Link href={"/"}>
            <a>
              <Image src={Logo} alt="logo" />
            </a>
          </Link>
          <div className={styles.center_link}>
            <Link href={"/Courses"}>
              <a>Courses</a>
            </Link>
            <Link href={"/"}>
              <a>Home</a>
            </Link>
            <a href="/block">Blog</a>
            <a href="/block">Contact</a>
          </div>
          <div className={styles.right_contaent}>
            <div className={styles.shp}>
              <Image className={styles.shp} src={shop} alt="shop" />
            </div>
            <p>
              Cart <span>0</span>
            </p>
            <div className={styles.right_btn}>
              <button>Sign up for Beta</button>
            </div>
          </div>
        </div>
      </header>
    </>
  );
};

export default Header;
