import React from "react";
import styles from "../../styles/Home.module.scss";
import Router from "next/router";

const BlackB = () => {
  const ExploreRouter = () => {
    Router.push("/insaetcurs");
  };

  return (
    <div className={styles.bg_yellow}>
      <div className="show_all">
        <button onClick={() => Router.push("/Courses")}>Show All Course</button>
      </div>
      <div className={styles.container_black}>
        <div className={styles.wrapper_block}>
          <div className={styles.left_block}>
            <div className={styles.block_text_bg}>
              <h1 className={styles.shit}>
                Curse<span>Top</span>
              </h1>
            </div>
            <h2>
              Level up your team's
              <br /> skills
            </h2>
            <p>
              Give your team the knowledge, experience, and confidence
              <br /> they need to tackle any problem.
            </p>
            <div className={styles.btn_left}>
              <button onClick={ExploreRouter}>Explore business plans</button>
            </div>
          </div>

          <div className={styles.right}>
            <img
              src="https://www.codecademy.com/webpack/a07f01c47acd236db2c717c148698e1f.svg"
              alt=""
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default BlackB;
