import React from "react";
import ReactPlayer from "react-player";
import BlockDown from "../ComputerS/BlockDown";

const ContaentComuterS = () => {
  return (
    <React.Fragment>
      <div className="bg">
        <div className="container_learn">
          <div className="box_flex">
            <div className="block_info_">
              <div className="player-wrapper">
                <ReactPlayer
                  className="react-player"
                  url="https://www.youtube.com/watch?v=-uleG_Vecis"
                  width="100%"
                  height="100%"
                  playing={true}
                  onReady={() => console.log("onReady shit")}
                  onStart={() => console.log("onStart callback")}
                  onError={() => console.log("onError callback")}
                  onEnded={() => console.log("onEnded callback")}
                />
              </div>
              <div className="contaent_course_stack">
                <h2>For those who want to</h2>
                <div className="box_stack_b">
                  <div className="child_box">
                    <h3>Python</h3>
                  </div>
                  <div className="child_box">
                    <h3>Command Line</h3>
                  </div>
                  <div className="child_box">
                    <h3>Git</h3>
                  </div>
                  <div className="child_box">
                    <h3>Data</h3>
                  </div>
                  <div className="child_box">
                    <h3>Structures....</h3>
                  </div>
                </div>
                <div className="box_text">
                  <p>For those who want to</p>
                  <ul>
                    <li>Study Computer Scince</li>
                    <li>Problem-slove like a pro</li>
                    <li>Start or advance in tech</li>
                  </ul>
                </div>
              </div>
            </div>
            {/* Left Contaent */}
            <div className="right_box_text">
              <h1>Computer Science</h1>
              <p className="shit">
                No matter what your coding goals are, the best place to start is by building a strong foundation. Similar to a college course, this Path will teach you how to think and code like a
                professional. You’ll learn Computer Science fundamentals, build a professional portfolio, and prepare for an entry-level role in tech.
              </p>
              <p>To start this Career Path, sign up for Codecademy Pro.</p>
              <div className="btn_si">
                <button>Try It For Free</button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <BlockDown />
    </React.Fragment>
  );
};

export default ContaentComuterS;
