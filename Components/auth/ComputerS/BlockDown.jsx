import React from "react";

const BlockDown = () => {
  return (
    <div className="block_bg_wrapper">
      <div className="box_wrapper">
        <div className="box_cont">
          <h1>Block</h1>
        </div>
      </div>
    </div>
  );
};

export default BlockDown;
