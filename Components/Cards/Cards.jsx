import React, { useState, useEffect } from "react";
import Image from "next/image";
import styles from "../../styles/Home.module.scss";
import Link from "next/link";
import staticsImg from "../Cards/img/st.svg";

const Cards = ({ posts }) => {
  const [posts, setPosts] = useState([]);

  useEffect(() => {
    async function load() {
      const responese = await fetch("http://localhost:4200/curses");
      const json = await responese.json();
      setPosts(json);
    }

    load();
  }, []);

  const [courses, setCourses] = useState([
    {
      id: Math.random(),
      name: "Development",
      price: "90$",
      img: "https://www.educative.io/cdn-cgi/image/f=auto,fit=cover,w=620/v2api/collection/4647042006646784/6412540532228096/image/4688907941183488",
    },
    {
      id: Math.random(),
      name: "Android Developer",
      price: "99$",
      img: "https://www.educative.io/cdn-cgi/image/f=auto,fit=cover,w=620/v2api/collection/6100126873419776/6055343571337216/image/4657441726791680",
    },
    {
      id: Math.random(),
      name: "Java Script Course",
      price: "50$",
      img: "https://www.educative.io/cdn-cgi/image/f=auto,fit=cover,w=620/v2api/collection/10370001/5016602356482048/image/4537483906252800",
    },
    {
      id: Math.random(),
      name: "PHP",
      price: "46$",
      img: "https://www.educative.io/cdn-cgi/image/f=auto,fit=cover,w=620/v2api/collection/10370001/5669965877215232/image/5493020851961856",
    },
    {
      id: Math.random(),
      name: "Node.js",
      price: "43$",
      img: "https://www.educative.io/cdn-cgi/image/f=auto,fit=cover,w=620/v2api/collection/10370001/5069947753988096/image/4730738324275200",
    },
    {
      id: Math.random(),
      name: "Docker",
      price: "59$",
      img: "https://www.educative.io/cdn-cgi/image/f=auto,fit=cover,w=620/v2api/collection/10370001/4574189490012160/image/5382736791994368",
    },
    {
      id: Math.random(),
      name: "Development",
      price: "90$",
      img: "https://www.educative.io/cdn-cgi/image/f=auto,fit=cover,w=620/v2api/collection/4647042006646784/6412540532228096/image/4688907941183488",
    },
    {
      id: Math.random(),
      name: "Android Developer",
      price: "99$",
      img: "https://www.educative.io/cdn-cgi/image/f=auto,fit=cover,w=620/v2api/collection/6100126873419776/6055343571337216/image/4657441726791680",
    },
    {
      id: Math.random(),
      name: "Java Script Course",
      price: "50$",
      img: "https://www.educative.io/cdn-cgi/image/f=auto,fit=cover,w=620/v2api/collection/10370001/5016602356482048/image/4537483906252800",
    },
  ]);

  return (
    <>
      <div className={styles.wrapper_flex}>
        {courses.map((item) => (
          <>
            <Link href={`/courses/${item.id}`}>
              <div className={styles.cards_curses} key={item.id}>
                <div className={styles.img_curs}>
                  <img className={styles.img_id} src={item.img} alt="shit" />
                </div>
                <div className={styles.statistick_curs_wrap}>
                  <Image src={staticsImg} alt="jjj" />
                </div>
                <div className={styles.line}>
                  <hr />
                </div>
                <div className={styles.info_curses_block}>
                  <h2>{item.name}</h2>
                  <span>{item.price}</span>
                </div>
                <div className={styles.info_curses}>
                  <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint nisi pariatur repellendus maiores saepe fugiat odio sunt expedita.</p>
                </div>
              </div>
            </Link>
          </>
        ))}
      </div>
    </>
  );
};

export default Cards;

Cards.getInitialProps = async () => {
  const response = await fetch("http://localhost:4200/curses");
  const posts = await response.json();

  return {
    props: {
      posts: posts,
    },
  };
};
