import React from "react";
import styles from "../../styles/Home.module.scss";
import Image from "next/image";
import imgBtn from "../TranformBlock/img/im.svg";
import rightImg from "../TranformBlock/img/Frame.svg";
import Alert from "../Alert/alert";

const Main = () => {
  const reloadPage = () => {
    alert("Shit");
  };

  return (
    <div className={styles.screen_wrapper}>
      <div className={styles.container_main}>
        <div className={styles.flex_screen}>
          <div className={styles.left_contaent}>
            <h1>
              <dic className={styles.bg_text}>
                <span>Learning makes</span>
              </dic>
              <br /> teamwork with
              <br /> a powerfu
              <br />l design tool
            </h1>
            <p>
              Stop wasting time with clunky, siloed teamwork. Get every
              stakeholder on the same page with a design tool that help you work
              better, faster.
            </p>
            <div className={styles.search_wrapper}>
              <input
                type={"search"}
                placeholder="What do you want to learn today?"
              />
              <button>Search</button>
            </div>
            <div className={styles.btn_main}>
              <button onClick={reloadPage}>Start your free trial</button>
              <div className={styles.imgBtn}>
                <Image src={imgBtn} />
              </div>
            </div>
          </div>

          <div className={styles.right_img}>
            <Image className={styles.img} src={rightImg} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Main;
