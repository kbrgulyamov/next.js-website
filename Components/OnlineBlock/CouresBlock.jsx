import React from "react";
import Image from "next/image";
import styles from "../../styles/Home.module.scss";
import Cards from "../Cards/Cards";

const AllCourses = () => {
  return (
    <div className={styles.bg_wrapper}>
      <div className={styles.container_bg}>
        <div className={styles.bg_title}>
          <h1>Our All The Online Courses</h1>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Placerat
            mauris non dictumst in
            <br /> leo. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
          </p>
        </div>
        <div className={styles.wrapper_cards}>
          <Cards />
        </div>
      </div>
    </div>
  );
};

export default AllCourses;
